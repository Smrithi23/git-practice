git commands used :

General git commands used:
git branch
git branch branchname
git checkout branchname
git status
git log

dev:
git add .
git commit -m "..."
git push origin dev
git checkout master
git merge dev

dev 2:
git add .
git commit --amend --no-edit
git push -f origin dev2

dev3:
git add .
git commit -m "...."
git push origin dev3

dev4:
git add .
git commit -m "..."
git push origin dev4

dev5
git revert -n id
git revert --continue
git push origin dev5

dev6
git branch algo
git checkout algo
git add .
git commit -m "..."
git push origin algo
git remote add <remote_name> <fork_url>
git remote -v
git fetch <remote_name>
git merge <remote_name>/<remote_branch> <branch>
//resolve conflicts if any
git add .
git commit -m "..."
git push origin algo
